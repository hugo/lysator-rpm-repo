Name:      lysator-repo
Version:   1.0.0
Release:   0
Summary:   Meta-package for adding Lysator local repository
License:   GPL
URL:       https://git.lysator.liu.se/hugo/lysator-repo
BuildArch: noarch
Source:    %{expand:%%(pwd)}
Prefix:    %{_prefix}
Packager:  Hugo Hörnquist <hugo@lysator.liu.se>

%description
Meta-package for adding Lysator local repository

%prep
# https://stackoverflow.com/questions/31930272/creating-rpm-using-source-file-in-current-directory
find . -mindepth 1 -delete
cp -af %{SOURCEURL0}/. .

%install
mkdir -p $RPM_BUILD_ROOT/etc/yum.repos.d
cp lysator.repo $RPM_BUILD_ROOT/etc/yum.repos.d/lysator.repo

%files
/etc/yum.repos.d/lysator.repo
