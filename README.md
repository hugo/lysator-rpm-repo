Lysator Repo RPM
================

The ambition of this repo is to create an RPM package which manages Lysators
repo in `yum.repos.d`. It currently however doesn't work.

Instead, the repo file is pushed directly through puppet, see
[lysator/puppet/lysrepo](https://git.lysator.liu.se/lysator/puppet/lysrepo/-/blob/master/manifests/init.pp)

This repo would however still be of use for GitLab runners.
